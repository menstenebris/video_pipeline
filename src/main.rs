extern crate opencv;
use crate::opencv::prelude::Vector;
//use std::sync::{Arc, Mutex};
use std::time::SystemTime;
use opencv::{core, videoio, imgproc};
use opencv::types::{VectorOfint};

#[macro_use] extern crate serde_derive;
extern crate serde;
extern crate serde_json;
//use serde_json::json;
use serde_json::{Value};
use serde::{Deserialize, Deserializer};
//use serde::de::Error;

extern crate chrono;
use chrono::{DateTime, Duration, Utc};
use chrono::serde::ts_milliseconds;

extern crate gstreamer as gst;
extern crate gstreamer_app as gst_app;
extern crate failure;
extern crate glib;

use failure::Error;
use gst::prelude::*;

#[macro_use]
extern crate failure_derive;

#[derive(Debug, Fail)]
#[fail(display = "Missing element {}", _0)]
struct MissingElement(&'static str);

#[derive(Debug, Copy, Clone)]
struct BBox { l: u64, b: u64, r: u64, t: u64 }

#[derive(Debug, Copy, Clone)]
struct Region {
    fps: u64,
    bbox: BBox,
    exit: bool
}

#[derive(Serialize, Debug)]
struct Header {
    id: u64,
    shape: [i32; 2],
    format: String,
    #[serde(with = "ts_milliseconds")]
    timestamp: DateTime<Utc>,
    camera: String,
}


struct Camera {
    pipe: gst::Pipeline,
    main_loop: glib::MainLoop,
}

impl Camera {
    fn new(location: &str) -> Camera {
        let label = String::from("label");
        let ctx: zmq::Context = zmq::Context::new();
        let socket = Camera::make_socket(label.clone().as_str(), &ctx);
        Camera {
            pipe: Camera::create_pipeline(location, socket).unwrap(),
            main_loop: glib::MainLoop::new(None, false),
        }
    }
    fn run(&self) -> Result<(), Error> {
        self.create_bus()?;
        self.pipe.set_state(gst::State::Playing)?;
        self.main_loop.run();
        Ok(())
    }
    fn make_socket(label: &str, ctx: &zmq::Context) -> zmq::Socket {
        let socket = ctx.socket(zmq::PUB).unwrap();
        socket
            .bind(format!("ipc://{}", label).as_str())
            .expect("cant connect to socket");
        socket
    }
    fn create_bus(&self) -> Result<(), Error>{
        let bus = self.pipe.get_bus().expect("Pipeline without bus. Shouldn't happen!");
        let ml = self.main_loop.clone();
        let pipe = self.pipe.clone();
        bus.add_watch(move |_: &gst::Bus, msg: &gst::Message| {
            use gst::MessageView;
            match msg.view() {
                MessageView::Eos(..) => {
                    pipe.set_state(gst::State::Null).unwrap();
                    ml.quit();
                },
                MessageView::Error(err) => {
                    println!(
                        "Error from {:?}: {} ({:?})",
                        err.get_src().map(|s| s.get_path_string()),
                        err.get_error(),
                        err.get_debug()
                    );
                    pipe.set_state(gst::State::Null).unwrap();
                    ml.quit();
                }
                _ => (),
            };
            glib::Continue(true)
        });
        Ok(())
    }
    fn create_pipeline(location: &str, socket: zmq::Socket) -> Result<gst::Pipeline, Error> {
        gst::init()?;

        let src = gst::ElementFactory::make("filesrc", Some("src"))
            .ok_or(MissingElement("cant create filesource"))?;
        let demux = gst::ElementFactory::make("qtdemux", Some("demux")).ok_or(MissingElement("cant create demux"))?;
        let parse = gst::ElementFactory::make("h264parse", Some("parse")).ok_or(MissingElement("cant create parse"))?;
        let decode = gst::ElementFactory::make("avdec_h264", Some("decode")).ok_or(MissingElement("cant create decodebin"))?;
        let convert = gst::ElementFactory::make("videoconvert", Some("convert")).ok_or(MissingElement("cant create convert"))?;
        let sink = gst::ElementFactory::make("appsink", Some("appsink")).ok_or(MissingElement("cant create appsink"))?;

        src.set_property("location", &location)?;

        let pipeline = gst::Pipeline::new(None);
        pipeline.add_many(&[&src, &demux, &parse, &decode, &convert, &sink])?;
        src.link(&demux)?;
        parse.link(&decode)?;
        decode.link(&convert)?;
        convert.link(&sink)?;

        let sink_pad = parse.get_static_pad("sink").unwrap();
        demux.connect_pad_added(move |_dbin, src_pad| {
            src_pad.link(&sink_pad).expect("Not linked");
        });

        let appsink = sink.dynamic_cast::<gst_app::AppSink>()
            .expect("Sink element is expected to be an appsink!");
        appsink.set_emit_signals(true);
        appsink.set_max_buffers(1);
        appsink.set_drop(true);
        appsink.set_wait_on_eos(false);
        appsink.set_property("sync", &false)?;
        appsink.set_callbacks(
            gst_app::AppSinkCallbacks::new()
                .new_sample(move |appsink: &gst_app::AppSink| {
                    let sample = appsink.pull_sample().ok_or(gst::FlowError::Eos)?;
                    let buffer = sample.get_buffer().ok_or_else(|| gst::FlowError::Error)?;
                    let caps = sample.get_caps().ok_or_else(|| gst::FlowError::Error)?;
                    let map = buffer.map_readable().ok_or_else(|| gst::FlowError::Error)?;
                    let samples = map.as_slice();
                    let structure = caps.get_structure(0).ok_or_else(|| gst::FlowError::Error)?;
                    let height: i32 = structure.get("height").expect("frame height is missed");
                    let width: i32 = structure.get("width").expect("frame width is missed");
                    let format: &str = structure.get("format").expect("frame format is missed");
                    let dims = VectorOfint::from_iter(vec![1080+1080/2, 1920]);
                    let frame = core::Mat::from_slice(samples).unwrap().reshape_nd(1, &dims).unwrap();
                    let mut rgb = core::Mat::default().unwrap();
                    imgproc::cvt_color(&frame, &mut rgb, imgproc::COLOR_YUV2RGB_I420, 3).unwrap();
//                    println!("{:#?}", rgb.size());
//                    let header = Header{
//                        id: buffer.get_pts().nseconds().unwrap(),
//                        shape: [width, height],
//                        format: String::from(format),
//                        timestamp: Utc::now(),
//                        camera: String::from("label")
//                    };
//                    let serialized_header = serde_json::to_string(&header).unwrap();
//                    let message = [
//                        b"label",
//                        samples,
//                        serialized_header.as_bytes()
//                    ];
//                    socket.send_multipart(message.iter(), 0).unwrap();
                    Ok(gst::FlowSuccess::Ok)
                })
                .build()
        );
        Ok(pipeline)
    }
}


fn bench_gstreamer(filename: &str) -> Result<(), ()> {
    let camera = Camera::new(filename);
    camera.run().expect("Loop stopped");
    Ok(())
}


fn bench_opencv(filename: &str) -> opencv::Result<()> {
    let mut cam = videoio::VideoCapture::new_from_file_with_backend(filename, videoio::CAP_ANY)?;
    let opened = videoio::VideoCapture::is_opened(&cam)?;
    if !opened { panic!("Unable to open default camera!") };
    let mut frame = core::Mat::default()?;
    let mut rgb = core::Mat::default()?;
    loop {
        cam.read(&mut frame)?;
        if frame.size()?.width > 0 {
            let a = 1;
//            println!("{:#?}", frame.size());
//            imgproc::cvt_color(&frame, &mut rgb, imgproc::COLOR_BGR2RGB, 0)?;
        }
        else {
            break
        }
    }
    Ok(())
}


fn main() {
    let num = 10;
    let filename = "Tractor_500kbps_x264.mp4";
//    let filename = "tos-1720x720-cfg01.mkv";
    let now = SystemTime::now();
    for _ in 0..num {
        bench_opencv(&filename).unwrap();
//        bench_gstreamer(&filename).unwrap();
    }
    let stop_time = (now.elapsed().unwrap().as_millis() as f64) / 1000 as f64;
    println!("{:?} {:?} {:?}", stop_time, stop_time/num as f64, (252*num) as f64/stop_time);
}