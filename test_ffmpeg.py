from timeit import timeit
from subprocess import Popen, PIPE, DEVNULL

import numpy as np
import cv2


def test_pure(source: str) -> None:
    width, height = (1920, 1080)
    # width, height = (1280, 720)
    stream_url = f'ffmpeg -vcodec h264 -i {source} -f rawvideo -pix_fmt yuv420p pipe:'.split(' ')
    with Popen(stream_url, stdout=PIPE, stderr=DEVNULL) as p:
        while p.stdout.readable():
            yuv_height = int(height+height//2)
            raw_frame = p.stdout.read(yuv_height * width)
            if len(raw_frame) == yuv_height * width:
                pass
            else:
                break
        p.stdout.flush()


def test_native_gray(source: str) -> None:
    width, height = (1920, 1080)
    # width, height = (1280, 720)
    stream_url = f'ffmpeg -vcodec h264 -i {source} -f rawvideo -pix_fmt gray8 pipe:'.split(' ')
    with Popen(stream_url, stdout=PIPE, stderr=DEVNULL) as p:
        while p.stdout.readable():
            raw_frame = p.stdout.read(height * width)
            if len(raw_frame) == height * width:
                np.frombuffer(raw_frame, np.uint8).reshape((height, width))
            else:
                break
        p.stdout.flush()


def test_native_rgb(source: str) -> None:
    width, height = (1920, 1080)
    # width, height = (1280, 720)
    stream_url = f'ffmpeg -vcodec h264 -i {source} -f rawvideo -pix_fmt rgb24 pipe:'.split(' ')
    with Popen(stream_url, stdout=PIPE, stderr=DEVNULL) as p:
        while p.stdout.readable():
            raw_frame = p.stdout.read(height * width * 3)
            if len(raw_frame) == height * width * 3:
                np.frombuffer(raw_frame, np.uint8).reshape((height, width, 3))
            else:
                break
        p.stdout.flush()


def test_rgb(source: str) -> None:
    width, height = (1920, 1080)
    # width, height = (1280, 720)
    stream_url = f'ffmpeg -vcodec h264 -i {source} -f rawvideo -pix_fmt yuv420p pipe:'.split(' ')
    with Popen(stream_url, stdout=PIPE, stderr=DEVNULL) as p:
        while p.stdout.readable():
            yuv_height = int(height+height//2)
            raw_frame = p.stdout.read(yuv_height * width)
            if len(raw_frame) == yuv_height * width:
                frame = np.frombuffer(raw_frame, np.uint8).reshape((yuv_height, width))
                cv2.cvtColor(frame, cv2.COLOR_YUV2RGB_I420)
            else:
                break
        p.stdout.flush()


def test_gray8(source: str) -> None:
    width, height = (1920, 1080)
    # width, height = (1280, 720)
    stream_url = f'ffmpeg -vcodec h264 -i {source} -f rawvideo -pix_fmt yuv420p pipe:'.split(' ')
    with Popen(stream_url, stdout=PIPE, stderr=DEVNULL) as p:
        while p.stdout.readable():
            yuv_height = int(height+height//2)
            raw_frame = p.stdout.read(yuv_height * width)
            if len(raw_frame) == yuv_height * width:
                frame = np.frombuffer(raw_frame, np.uint8).reshape((yuv_height, width))
                cv2.cvtColor(frame, cv2.COLOR_YUV2GRAY_I420)
            else:
                break
        p.stdout.flush()


def test_yuv_gray(source: str) -> None:
    width, height = (1920, 1080)
    # width, height = (1280, 720)
    stream_url = f'ffmpeg -vcodec h264 -i {source} -f rawvideo -pix_fmt yuv420p pipe:'.split(' ')
    with Popen(stream_url, stdout=PIPE, stderr=DEVNULL) as p:
        while p.stdout.readable():
            raw_frame = p.stdout.read(height * width)
            p.stdout.read(height // 2 * width)
            if len(raw_frame) == height * width:
                np.frombuffer(raw_frame, np.uint8).reshape((height, width))
            else:
                break
        p.stdout.flush()


def test_fake(source: str) -> None:
    stream_url = f'ffmpeg -vcodec h264 -i {source} -f null /dev/null'.split(' ')
    p = Popen(stream_url, stdout=PIPE)
    p.stdout.flush()


if __name__ == '__main__':
    stream_url = 'Tractor_500kbps_x264.mp4'
    # stream_url = 'tos-1720x720-cfg01.mkv'

    num = 10
    results = {
        'pure       ': timeit('test_pure(stream_url)', globals=globals(), number=num),
        'yuv_gray   ': timeit('test_yuv_gray(stream_url)', globals=globals(), number=num),
        'yuv_gray8  ': timeit('test_gray8(stream_url)', globals=globals(), number=num),
        'yuv_rgb    ': timeit('test_rgb(stream_url)', globals=globals(), number=num),
        'native_gray': timeit('test_native_gray(stream_url)', globals=globals(), number=num),
        'native_rgb ': timeit('test_native_rgb(stream_url)', globals=globals(), number=num),
    }
    print('Всего времени | На итерацию | fps')
    for name, result in results.items():
        print(f'{name} ->   {result:.4f}с | {result/num:.4f}с | {(252*num)/result:.4f} fps')
