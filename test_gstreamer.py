from timeit import timeit

import numpy as np
import cv2
import gi

gi.require_version('Gst', '1.0')
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')

from gi.repository import GLib, Gst


def bus_call(bus, message, loop, pipe):
    t = message.type
    if t == Gst.MessageType.EOS:
        # print('End-of-stream')
        pipe.set_state(Gst.State.NULL)
        loop.quit()
    elif t == Gst.MessageType.ERROR:
        err, debug = message.parse_error()
        print(f'{err}: {debug}')
        pipe.set_state(Gst.State.NULL)
        loop.quit()
    return Gst.FlowReturn.OK


def pure(appsink):
    sample = appsink.emit("pull-sample")
    buf = sample.get_buffer()
    caps = sample.get_caps()
    height = caps.get_structure(0).get_value('height')
    width = caps.get_structure(0).get_value('width')
    stream_format = caps.get_structure(0).get_value('format')
    data = buf.extract_dup(0, buf.get_size())
    return Gst.FlowReturn.OK


def yuv_rgb(appsink):
    sample = appsink.emit("pull-sample")
    buf = sample.get_buffer()
    caps = sample.get_caps()
    height = caps.get_structure(0).get_value('height')
    width = caps.get_structure(0).get_value('width')
    stream_format = caps.get_structure(0).get_value('format')
    data = buf.extract_dup(0, buf.get_size())
    if data:
        frame = np.frombuffer(data, np.uint8).reshape((height+height//2, width))
        cv2.cvtColor(frame, cv2.COLOR_YUV2RGB_I420)
    return Gst.FlowReturn.OK


def yuv_gray8(appsink):
    sample = appsink.emit("pull-sample")
    buf = sample.get_buffer()
    caps = sample.get_caps()
    height = caps.get_structure(0).get_value('height')
    width = caps.get_structure(0).get_value('width')
    stream_format = caps.get_structure(0).get_value('format')
    data = buf.extract_dup(0, buf.get_size())
    if data:
        frame = np.frombuffer(data, np.uint8).reshape((height+height//2, width))
        cv2.cvtColor(frame, cv2.COLOR_YUV2GRAY_I420)
    return Gst.FlowReturn.OK


def yuv_gray(appsink):
    sample = appsink.emit("pull-sample")
    buf = sample.get_buffer()
    caps = sample.get_caps()
    height = caps.get_structure(0).get_value('height')
    width = caps.get_structure(0).get_value('width')
    stream_format = caps.get_structure(0).get_value('format')
    data = buf.extract_dup(0, height*width)
    buf.extract_dup(0, (height//2)*width)
    if data:
        np.frombuffer(data, np.uint8).reshape((height, width))
    return Gst.FlowReturn.OK


def native_rgb(appsink):
    sample = appsink.emit("pull-sample")
    buf = sample.get_buffer()
    caps = sample.get_caps()
    height = caps.get_structure(0).get_value('height')
    width = caps.get_structure(0).get_value('width')
    stream_format = caps.get_structure(0).get_value('format')
    data = buf.extract_dup(0, buf.get_size())
    if data:
        np.frombuffer(data, np.uint8).reshape((height, width, 3))
    return Gst.FlowReturn.OK


def native_gray(appsink):
    sample = appsink.emit("pull-sample")
    buf = sample.get_buffer()
    caps = sample.get_caps()
    height = caps.get_structure(0).get_value('height')
    width = caps.get_structure(0).get_value('width')
    stream_format = caps.get_structure(0).get_value('format')
    data = buf.extract_dup(0, buf.get_size())
    if data:
        np.frombuffer(data, np.uint8).reshape((height, width))
    return Gst.FlowReturn.OK

def test_zmq(appsink):
    sample = appsink.emit("pull-sample")
    buf = sample.get_buffer()
    caps = sample.get_caps()
    height = caps.get_structure(0).get_value('height')
    width = caps.get_structure(0).get_value('width')
    stream_format = caps.get_structure(0).get_value('format')
    data = buf.extract_dup(0, buf.get_size())
    if data:
        np.frombuffer(data, np.uint8).reshape((height, width))
    return Gst.FlowReturn.OK


def pipe_init(source, on_new_sample, pix_format):
    Gst.init(None)
    pipe = Gst.Pipeline.new('dynamic')

    src = Gst.ElementFactory.make('filesrc')
    demux = Gst.ElementFactory.make('qtdemux')
    parse = Gst.ElementFactory.make('h264parse')
    decode = Gst.ElementFactory.make('avdec_h264')
    convert = Gst.ElementFactory.make('videoconvert')
    sink = Gst.ElementFactory.make('appsink')

    for item in (src, demux, parse, decode, convert, sink):
        pipe.add(item)

    src.link(demux)
    demux.connect('pad-added', lambda element, pad: element.link(parse))
    parse.link(decode)
    decode.link(convert)
    convert.link(sink)

    src.set_property('location', source)

    sink.set_property("emit-signals", True)
    sink.set_property("max-buffers", 1)
    caps = Gst.caps_from_string(f"video/x-raw, format=(string){pix_format}")
    sink.set_property("caps", caps)
    sink.set_property("drop", True)
    sink.set_property("wait-on-eos", True)
    sink.set_property('sync', False)
    sink.connect("new-sample", on_new_sample)
    return pipe


def run(source, sink_callback, pix_format):
    loop = GLib.MainLoop()
    pipe = pipe_init(source, sink_callback, pix_format)

    bus = pipe.get_bus()
    bus.add_signal_watch()
    bus.enable_sync_message_emission()
    bus.connect('message', bus_call, loop, pipe)

    pipe.set_state(Gst.State.PLAYING)
    loop.run()


if __name__ == '__main__':
    source = 'Tractor_500kbps_x264.mp4'
    # stream_url = 'tos-1720x720-cfg01.mkv'
    # run(source, pure)
    num = 10
    results = {
        'pure       ': timeit('run(source, pure, "I420")', globals=globals(), number=num),
        # 'yuv_gray   ': timeit('run(source, yuv_gray, "I420")', globals=globals(), number=num),
        # 'yuv_gray8  ': timeit('run(source, yuv_gray8, "I420")', globals=globals(), number=num),
        # 'yuv_rgb    ': timeit('run(source, yuv_rgb, "I420")', globals=globals(), number=num),
        # 'native_gray': timeit('run(source, native_gray, "GRAY8")', globals=globals(), number=num),
        # 'native_rgb ': timeit('run(source, native_rgb, "RGB")', globals=globals(), number=num),
    }

    print('           Всего времени | На итерацию | fps')
    for name, result in results.items():
        print(f'{name} ->   {result:.4f}с | {result/num:.4f}с | {(252*num)/result:.4f} fps')
