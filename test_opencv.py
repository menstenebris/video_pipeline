from timeit import timeit
import pickle

import cv2
import numpy as np


def test_pure(source: str) -> None:
    cap = cv2.VideoCapture(source)
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break
    cap.release()


def test_rgb(source: str) -> None:
    cap = cv2.VideoCapture(source)
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break
        pickle.loads(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB).dumps())
    cap.release()


def test_gray(source: str) -> None:
    cap = cv2.VideoCapture(source)
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break
        pickle.loads(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY).dumps())
    cap.release()


if __name__ == '__main__':
    stream_url = 'Tractor_500kbps_x264.mp4'
    # stream_url = 'tos-1720x720-cfg01.mkv'

    num = 10
    results = {
        'pure ': timeit('test_pure(stream_url)', globals=globals(), number=num),
        'rgb  ': timeit('test_rgb(stream_url)', globals=globals(), number=num),
        'gray8': timeit('test_gray(stream_url)', globals=globals(), number=num),
    }

    print('           Всего времени | На итерацию | fps')
    for name, result in results.items():
        print(f'{name} ->   {result:.4f}с | {result/num:.4f}с | {(252*num)/result:.4f} fps')
